import React from 'react';
import SafeAreaView from 'react-native-safe-area-view';
import { Constants } from 'expo';
import {
    StreamApp,
    FlatFeed,
    Activity,
    LikeButton,
    StatusUpdateForm,
} from 'expo-activity-feed';


function createUserSession(userId): UserSession {
  return stream.connect(apiKey, stream.signing.JWTUserSessionToken(apiSecret, userId), appId);
}

const CustomActivity = (props) => {
  // Console devise name
  console.log(Constants.deviceName)
  return (
    <Activity
      {...props}
      Footer={
        <LikeButton {...props} />
      }
    />
  );
};

const App = () => {
  // add one of the devise name here, and two tokens generated here
  var token = (Constants.deviceName == 'D2502')? 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZmx1ZmYifQ.vFly4xWltySc8IawOf_QJYyszPCIJgk7rPGaVxIRRB4' : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiam9obiJ9.14yF9WIDllGI0_58fZFzLriWyAxMRj6GGU5MnQI5WJQ'
  return (
    <SafeAreaView style={{flex: 1}} forceInset={{ top: 'always' }}>
      <StreamApp
          apiKey="q7szvrfb8hmr"
          appId="51557"
          token={token}
      >
        <FlatFeed Activity={CustomActivity} notify />
        <StatusUpdateForm feedGroup="timeline" />
      </StreamApp>
    </SafeAreaView>
  );
};

export default App;