// @flow

import stream from 'getstream';
import faker from 'faker';

import type { UserSession, CloudClient } from '../types';

main();
async function main() {
  let apiKey = 'q7szvrfb8hmr';
  let apiSecret = 'kturz8st2uk3erqwp4efjtntbqrjukwu42pvktt5s3c8ydxtvwbaau3f8enzj76w';
  let appId = '51557';
  console.log('appId', appId)
  if (!apiKey) {
    console.error('STREAM_API_KEY should be set');
    return;
  }

  if (!appId) {
    console.error('STREAM_APP_ID should be set');
    return;
  }

  if (!apiSecret) {
    console.error('STREAM_SECRET should be set');
    return;
  }

  function createUserSession(userId): UserSession {
    return stream.connect(apiKey, stream.signing.JWTUserSessionToken(apiSecret, userId), appId);
  }

  let batman = createUserSession('batman');
  let fluff = createUserSession('fluff');
  let john = createUserSession('john');

  await fluff.feed('user').follow('user', batman.currentUser);
  await batman.feed('user').follow('user', fluff.currentUser);
  await fluff.feed('user').follow('user', john.currentUser);
  await john.feed('user').follow('user', fluff.currentUser);

  // await fluff.follow('user', 'john');
  // await john.follow('user', 'fluff');

  client = stream.connect('q7szvrfb8hmr', 'kturz8st2uk3erqwp4efjtntbqrjukwu42pvktt5s3c8ydxtvwbaau3f8enzj76w', '51557');
  var fluff_timeline = client.feed('timeline', 'fluff');
  fluff_timeline.follow('user', 'john').then(
    function(err) {
    }
  );
  var john_timeline = client.feed('timeline', 'john');
  john_timeline.follow('user', 'fluff').then(
    function(err) {
    }
  );
  var fluff_timeline = client.feed('timeline', 'fluff');
  fluff_timeline.follow('timeline', 'john').then(
    function(err) {
    }
  );
  var john_timeline = client.feed('timeline', 'john');
  john_timeline.follow('timeline', 'fluff').then(
    function(err) {
    }
  );

  console.log('Add the following line to your App.js file');
  console.log('fluff', 'STREAM_API_TOKEN=' + fluff.userToken);
  console.log('john', 'STREAM_API_TOKEN=' + john.userToken);

  await batman.currentUser.getOrCreate({
    name: 'Batman',
    url: 'batsignal.com',
    desc: 'Smart, violent and brutally tough solutions to crime.',
    profileImage:
      'https://i.kinja-img.com/gawker-media/image/upload/s--PUQWGzrn--/c_scale,f_auto,fl_progressive,q_80,w_800/yktaqmkm7ninzswgkirs.jpg',
    coverImage:
      'https://i0.wp.com/photos.smugmug.com/Portfolio/Full/i-mwrhZK2/0/ea7f1268/X2/GothamCity-X2.jpg?resize=1280%2C743&ssl=1',
  });

  await fluff.currentUser.getOrCreate({
    name: 'Fluff',
    url: 'fluff.com',
    desc: 'Sweet I think',
    profileImage:
      'https://mylittleamerica.com/988-large_default/durkee-marshmallow-fluff-strawberry.jpg',
    coverImage: '',
  });

  await john.currentUser.getOrCreate({
    name: 'John',
    url: 'John.com',
    desc: 'Sweet I think i am John',
    profileImage:
      'https://i.kinja-img.com/gawker-media/image/upload/s--PUQWGzrn--/c_scale,f_auto,fl_progressive,q_80,w_800/yktaqmkm7ninzswgkirs.jpg',
    coverImage: '',
  });

  let randomUsers = [];
  let randomUsersPromises = [];
  for (let i = 0; i < 30; i++) {
    let session = createUserSession(`random-${i}`);
    randomUsers.push(session);
    randomUsersPromises.push(
      session.currentUser.getOrCreate({
        name: faker.name.findName(),
        profileImage: faker.internet.avatar(),
        desc: faker.lorem.sentence(),
      }),
    );
  }
  await Promise.all(randomUsersPromises);

  await ignore409(() =>
  Promise.all(
    randomUsers.slice(11, 27).map((user, i) =>
      user.feed('notification', john.currentUser.id).addActivity({
        foreign_id: 'follow:john-random-' + i,
        time: '2018-08-10T13:12:' + i,

        actor: user.currentUser,
        verb: 'follow',
        object: john.currentUser,
      }),
    ),
  ),
);
}

async function ignore409(asyncfn) {
  try {
    await asyncfn();
  } catch (e) {
    if (
      !(e instanceof stream.errors.StreamApiError) ||
      e.response.statusCode != 409
    ) {
      throw e;
    }
  }
}